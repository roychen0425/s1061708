﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1061708.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["aaa"] = 111;
            ViewData["A"] = 1;
            ViewData["B"] = 2;
            ViewBag.name = "Roy";
            ViewBag.A = 1;
            ViewBag.B = 2;
            return View();
        }
        public ActionResult HTML()
        {
            return View();
        }
        public ActionResult HtmlHelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
    }
}