﻿using s1061708.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1061708.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }
        [HttpPost]
        public ActionResult Index(BMIData data)
        {

            if (ModelState.IsValid)
            {
                var m_height = data.Height/100;
                var Result = (data.Weight / (m_height * m_height));
                var level = "";
                if(Result < 18.5)
                {
                    level = "體重過輕";
                }
                else if(18.5<= Result && Result < 24)
                {
                    level = "正常範圍";
                }
                else if (24 <= Result && Result < 27)
                {
                    level = "過重";
                }
                else if (27 <= Result && Result < 30)
                {
                    level = "輕度肥胖";
                }
                else if (30 <= Result && Result < 35)
                {
                    level = "中度肥胖";
                }
                else if (35 <= Result)
                {
                    level = "重度肥胖";
                }
                data.Result = Result;
                data.Level = level;
            }

            return View(data);
        }
    }
}